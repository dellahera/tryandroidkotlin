package binar.app.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import binar.app.BinarApp
import binar.app.BuildConfig
import binar.app.R
import binar.app.adapter.SiswaAdapter
import binar.app.model.Siswa
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.activity_recycler_view.*
import binar.app.common.toastku

class MainActivity : AppCompatActivity() {
    private val siswaList = mutableListOf<Siswa>()
    private val siswaAdapter = SiswaAdapter(siswaList, {
        toastku("Click ${it.name}")
    }, {
        toastku("Long Click ${it.name}")
    })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycler_view)

//        tv1Hello.text = "Hello ${BinarApp.sp.nama}"
//        btnLogout.setOnClickListener {
//            BinarApp.sp.Logout()
//            startActivity(Intent(this, LoginActivity::class.java))
//            finish()
//        }
        setUpView()
        getSiswa()

        val url = if (BuildConfig.DEBUG)
            "Mode Debug : ${BuildConfig.BASE_URL}"
            else "Mode Release : ${BuildConfig.BASE_URL}"
    }

    private fun getSiswa() {
        (1..15).forEach() { index ->
            val siswa = Siswa("Student $index", "student$index@gmail.com")
            siswaList.add(siswa)
        }
        siswaAdapter.notifyDataSetChanged()
    }

    private fun setUpView() {
        rvBelajar.layoutManager = LinearLayoutManager(this)
        rvBelajar.adapter = siswaAdapter
    }
}
