package binar.app.activity

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import binar.app.BinarApp
import binar.app.R
import binar.app.common.Constant
import binar.app.data.PreferenceHelper
import kotlinx.android.synthetic.main.activity_login.*

class LoginActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupView()

     /*   val sp: PreferenceHelper= BinarApp.sp
//        sp.putString(Constant.NAMA, "Della")
//        sp.putString(Constant.IG, "Della_hera")
//
//        val nama: String = sp.getString(Constant.NAMA)
       val ig : String = sp.getString(Constant.IG) ?: "HE HE"
 //key harus berbeda beda
        sp.nama= "della"
        println("Nama : ${sp.nama}")
        println("ig : $ig")*/
    }

    private fun cekUser(){
        val name= BinarApp.sp.nama
        if(name.isNotBlank()){
            startActivity(Intent(this, MainActivity::class.java ))
        }
    }
    private fun setupView() {
        btnLogin.setOnClickListener {
            login()
        }
    }

    private fun login() {
        val username = etUsername.text.toString()
        val password = etPassword.text.toString()

        if (username=="della" && password=="hera"){
            showMessage("Login sukses")
            BinarApp.sp.nama= username
            startActivity(Intent(this, MainActivity::class.java))
            finish()
        } else {
           showMessage("Username atau password salah")
        }
    }

    private fun showMessage(message: String){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show()
    }
}
